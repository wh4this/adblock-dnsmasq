#as root
if [ "$EUID" -ne 0 ]
  then echo "Please run as root"
  exit
fi

cp update_bannerhosts /usr/local/bin
chown root.root /usr/local/bin/update_bannerhosts
chmod 700 /usr/local/bin/update_bannerhosts
/usr/local/bin/update_bannerhosts


touch /etc/cron.d/update_bannerhosts
echo "#Update the banner hosts...
0 0,4,8,12,16,18,20 * * * root /usr/local/bin/update_bannerhosts" > /etc/cron.d/update_bannerhosts
exit